<?php

use App\Containers\Backoffice\Sector\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/sector', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the sector post
Route::get('{lang}/sector/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the sector post
Route::put('{lang}/sector/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the sector post
Route::post('{lang}/sector/create', [Controller::class, 'createPost'])->middleware(['auth:api']);