<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\File;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersFilemanager;
use Olmo\Core\App\Http\Controller\BackController;

class HelpersSlider
{

    private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

    private const CONTAINER_SLIDER = 'Slider';

    public static function createNewSlide($request, $attribute)
    {

        $lang = $request->lang;
        $model = $request->input('model');
        $column = $request->input('name');
        $id = $request->input('id');

        /**
         * Get the current columns in the requested table
         */
        $columns = Schema::getColumnListing($attribute['table']);
        /** Flip values in the keys */
        $columns_flip = array_flip($columns);
        /** Set every value to empty string with exceptions */
        array_walk($columns_flip, function(&$v, $k, $lang){
            if(HelpersData::setLabel($k) == 'locale'){
                $v = $lang;  
            } else if(HelpersData::getType($k) == 'readonly' || HelpersData::getType($k) == 'select') {
                $v = null;
            } else if($k == 'created_at' || $k == 'updated_at'){
                $v = null;
            } else {
                $v = "";  
            }
        }, $lang);

        unset($columns_flip['id']);

        /**
         * Prefill some field to make the update possible
         */
        if(isset($columns_flip['model_hidden_content'])){
            $columns_flip['model_hidden_content'] = $model;
        }
        if(isset($columns_flip['postid_hidden_content'])){
            $columns_flip['postid_hidden_content'] = $id;
        }   
        if(isset($columns_flip['columnname_hidden_content'])){
            $columns_flip['columnname_hidden_content'] = $column;
        }                

        /** Insert the post in the DB */
        $response = Db::table($attribute['table'])->insertGetId($columns_flip);
        $response = array('id' => $response);

        return response($response, 200);        

    }

    /**
     * This method is a clone of BackController serializeSinglePost
     * but with few adjusments to fit with the slide options
     * it could be merged with the BackController serializeSinglePost 
     * but I think could be more clean and stable keep it alone
     */
    public static function serializeSingleSlide($request, $attribute)
    {
        $id = $request->id;
        $lang = $request->lang;
        $data = Db::table('olmo_slideritem')->where('id', $id)->first();

        if ($data) {

            unset($data->created_at);
            unset($data->updated_at);
    
            $data = self::cleanSliderField($data, $data->model_hidden_content, $data->columnname_hidden_content);
    
            $items = [];
            $i = 0;

            foreach ($data as $key => $value) {

                $item = [];

                $item['name'] = (string)$key;
                $item['label'] = HelpersData::setLabel((string)$key);;
                $item['tab'] = HelpersData::setTab((string)$key);
                $item['type'] = HelpersData::getType((string)$key, $key);
                $item['required'] = false;
                
                $item['values'] = BackController::getValues($key, (string)$value, $item['type'], $lang, $attribute['table'], $id);
                $item['value'] = BackController::getValue($key, (string)$value, $item['values'], $item['type'], $attribute['table'], $id);

                $items[$i] = $item;
                $i++;

            }

            return $items;

        } else {
            return response(404);
        }        

    }

    /**
     * Kind of tricky method, could be inproved
     * 
     * Get the 
     */
    public static function serializeSlideUpdating($request, $attribute, $dataPost)
    {

        $id = $dataPost['id'];

        $body = HelpersData::normalizeBody($request);
        Db::table($attribute['table'])->where('id', $id)->update($body);
        $data = Db::table($attribute['table'])->where('id', $id)->first();

        if ($data) {

            $postid = $data->postid_hidden_content;
            $model = $data->model_hidden_content;
            $column = $data->columnname_hidden_content;

            $table = 'olmo_'.$model;

            $getslides = Db::table($table)->where('id', $postid)->first();
            $convertToArray = (array)$getslides;

            $response = 0;

            if(strpos($convertToArray[$column], $id) === false){
                $changeSlides = $convertToArray[$column] == "" ? $id : $convertToArray[$column] . ',' . $id;
                $response = Db::table($table)->where('id', $postid)->update([$column => $changeSlides]);
            }

            return response($response, 200);

        } else {
            return response(404);
        }        

    }

    public static function cleanSliderField($data, $model, $label)
    {

        $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SLIDER . DIRECTORY_SEPARATOR);
        $directory = File::isDirectory($menuDirectory);
        $dataslider = null;
        $data = (array)$data;
    
        if ($directory) {
          $sliderPath = $menuDirectory . 'slider.json';
          $dataslider = json_decode(file_get_contents($sliderPath), true);
        }   
        
        $default = [
            "locale_hidden_content",
            "parentid_hidden_general",
            "model_hidden_content",
            "postid_hidden_content",
            "columnname_hidden_content",
            "name_txt_content",
            "primary_filemanager_content"            
        ];
    
        if(isset($dataslider[$model])) {

            foreach($dataslider[$model] as $slider){

                if($slider['slider'] == $label){                    
                    $activeField = array_merge($default, $slider['field']);
                    $differenceField = array_diff($dataslider['table'], $activeField);
        
                    foreach($differenceField as $item){
                        unset($data[$item]);
                    }
                }

            }

        }

        return $data;

    }

    public static function getSlides($value)
    {
        if($value == ''){
            return '';
        } else{
            $slides = explode(',',$value);
            foreach($slides as $key=>$item){
                $slides[$key] = self::getSlide($item);
            }
            return $slides;
        }        
    }

    public static function getSlide($value)
    {
        $slide = DB::table('olmo_slideritem')->where('id', $value)->select('id','name_txt_content as name','primary_filemanager_content as primary')->first();
        
        if($slide){
            $slide->primary = HelpersFilemanager::getSinglePathFmanagerById($slide->primary, 'media');
            return $slide;
        } else {
            return '';
        }
    }

}