<?php

namespace Olmo\Core\Containers\Backoffice\Menu\Controllers;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersPermission;

use Illuminate\Http\Request;

class Controller extends BackController
{

    public function getMenu(Request $request)
    {
      return HelpersPermission::getMenu($request);
    }    

}