<?php

namespace App\Containers\Backoffice\Sector\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class SectorRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
