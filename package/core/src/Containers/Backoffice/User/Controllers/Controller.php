<?php

namespace Olmo\Core\Containers\Backoffice\User\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\Containers\Backoffice\User\Models\User;
use Olmo\Core\App\Helpers\HelpersData;

class Controller extends BackController
{

    public function createUserSessionLogin(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $user = User::where('email_email_general', $username)->where('password_pwd_general', $password)
            ->select(
                'id',
                'email_email_general as email',
                'email_email_general as username',
                'role_id_general as role_id',
                'avatar_filemanager_general as avatar'
            )
            ->first();

        if (!$user) {
            return response(["User not found"], 404);
            exit();
        }

        $role = $this->DbRelation('olmo_role', $user->role_id)->name_txt_general;
        // $user['role'] = HelpersNormalize::normalizeObject($role);        

        // Generate the session token
        $token = md5(time() . 'OLMO');
        User::where('id', $user->id)->update(['token_hidden_general' => $token]);

        // Remove unused keys
        unset($user['id']);
        unset($user['role_id']);

        $user['token'] = $token;
        $user['role'] = $role;
        // $user['avatar'] = ApiHelper::getSinglePathStorageById($user['avatar']);

        return response(json_encode($user), 200);
    }

    public function createPost(Request $request)
    {
        $attribute = User::theAttributes();

        return $this->createNewPost($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = User::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }    

    public function getPost(Request $request)
    {
        $attribute = User::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return  $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = User::theAttributes();
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }
}