<?php

use App\Containers\Backoffice\Formatpers\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/formatpers', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the formatpers post
Route::get('{lang}/formatpers/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the formatpers post
Route::put('{lang}/formatpers/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the formatpers post
Route::post('{lang}/formatpers/create', [Controller::class, 'createPost'])->middleware(['auth:api']);