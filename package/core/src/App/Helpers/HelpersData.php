<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelpersData
{

    public static function normalizeBody($request)
    {

        /**
         * PLEASE FIX ME I SUCK!!
         * This method has not been modify, so take a moment to verify it
         * when all default models are done. 
         */

        //Normalizzo il body
        $body = $request->toArray();
        $params = [];
        foreach ($body as $key => $value) {
            if (is_array($value)) {
                //conrolla se esiste la chiave key
                if (isset($value['value'])) {
                    $params[$key] = @$value['value'];
                } else {
                    $rel = '';
                    foreach ($value as $v) {
                        $rel .= @$v['value'] . ',';
                    }
                    $params[$key] = rtrim($rel, ',');
                }
            } else {
                if (strpos($key, 'password') !== false) {
                    if ($value != '') {
                        $params[$key] = $value;
                    }
                } else if (strpos($key, '_is') !== false) {
                    // $params[$key] = $value;
                    if ($value == 1 || $value == "true" || $value == "1") {
                        $params[$key] = "true";
                    } else {
                        $params[$key] = "false";
                    }
                } else if (strpos($key, '_filemanager') !== false) {
                    $params[$key] = $value ? Db::table('olmo_storage')->where('truename', $value)->first()->id : "";
                } else {
                    if ($value == '') {
                        $value = "";
                    }
                    $params[$key] = $value;
                }
            }
        }

        return $params;
    }

    public static function isJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    public static function removeNullKey($data)
    {
        foreach($data as $key=>$value) {
            if($value === null){
                unset($data[$key]);
            }
        }

        return $data;
    }    

    public static function idLangPost(Request $request)
    {

        $item = [];

        $id = $request->id ? $request->id : 1;
        $lang = $request->lang;

        $item['id'] = $id;
        $item['lang'] = $lang;

        return $item;
    }

    public static function getType($field)
    {

        if (strpos($field, '_id_')) {
            $type      = 'select';
        } else if (strpos($field, '_multid_')) {
            $type  = 'multiselect';
        } else if (strpos($field, '_select_')     !== false) {
            $type = 'select';
        } else if (strpos($field, '_email_')      !== false) {
            $type = 'email';
        } else if (strpos($field, '_media_')      !== false) {
            $type = 'media';
        } else if (strpos($field, '_multimg_')    !== false) {
            $type = 'multiimage';
        } else if (strpos($field, '_img_')        !== false) {
            $type = 'singleimage';
        } else if (strpos($field, '_is_')         !== false) {
            $type = 'checkbox';
        } else if (strpos($field, '_editor_')     !== false) {
            $type = 'texteditor';
        } else if (strpos($field, '_txt_')        !== false) {
            $type = 'text';
        } else if (strpos($field, '_txtarea_')    !== false) {
            $type = 'textarea';
        } else if (strpos($field, '_num_')        !== false) {
            $type = 'number';
        } else if (strpos($field, '_date_')       !== false) {
            $type = 'date';
        } else if (strpos($field, '_hidden_')     !== false) {
            $type = 'hidden';
        } else if (strpos($field, '_list_')       !== false) {
            $type = 'list';
        } else if (strpos($field, '_ord_')        !== false) {
            $type = 'number';
        } else if (strpos($field, '_div_')        !== false) {
            $type = 'divider';
        } else if (strpos($field, '_down_')       !== false) {
            $type = 'downloader';
        } else if (strpos($field, '_spin_')       !== false) {
            $type = 'spinner';
        } else if (strpos($field, '_read_')       !== false) {
            $type = 'readonly';
        } else if (strpos($field, '_color_')      !== false) {
            $type = 'colorpicker';
        } else if (strpos($field, '_dnd_')        !== false) {
            $type = 'order';
        } else if (strpos($field, '_spin_')       !== false) {
            $type = 'spinner';
        } else if (strpos($field, '_pwd_')        !== false) {
            $type = 'password';
        } else if (strpos($field, '_slider_')     !== false) {
            $type = 'slider';
        } else if (strpos($field, '_visual_')     !== false) {
            $type = 'visual';
        } else if (strpos($field, '_label_')      !== false) {
            $type = 'label';
        } else if (strpos($field, '_props_')      !== false) {
            $type = 'multiselect';
        } else if (strpos($field, '_filemanager_') !== false) {
            $type = 'filemanager';
        } else if (strpos($field, '_langs_') !== false) {
            $type = 'lang';
        } else if (strpos($field, 'copy')         !== false) {
            $type = 'copy';
        } else if (strpos($field, '_property_')         !== false) {
            $type = 'property';
        } else {
            $type = 'hidden';
        }

        return (string)$type;
    }

    public static function setTypeList($value)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */
        $type = self::getType($value);
        // explode('_', $value)[0]

        if ($type == 'checkbox' || $type == 'enabled') {
            $type = 'label';
        } else if ($type == 'singleimage') {
            $type = 'image';
        } else if ($type == 'select' || $type == 'multiselect' || $type == 'role' || $type == 'text') {
            $type = 'text';
        } else if ($type == 'email') {
            $type = 'email';
        } else if ($type == 'lang') {
            $type = 'lang'; // delete it
        } else {
            $type = 'hidden';
        }

        return $type;
    }

    public static function setLabel($value)
    {
        if ($value == 'copytranslate') {
            $label = array(0 => 'Copy content post');
        } else {
            $label = explode('_', $value);
        }
        return isset($label[0]) ? $label[0] : $value;
    }

    public static function setTab($value)
    {
        $defaultTab = 'general';
        $tab = explode('_', $value);
        return isset($tab[2]) ? $tab[2] : $defaultTab;
    }

    public static function getDefaultValue($required, $k)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */
        $res = "";
        if (isset($required[$k])) {
            $res =  $required[$k];
        }
        return $res;
    }

    /**
     * This method is use inside the BackController serializeSinglePost
     * to print-out a required condition inside a field
     */
    public static function isRequired($required, $key)
    {
        $response = "false";
        if (is_array($required)) {
            if (in_array($key, $required)) {
                $response = "true";
            }
        }
        return $response;
    }

    public static function currentValueSelect($collection, $value)
    {
        foreach ($collection as $item) {
            if ($item['value'] == $value) {
                $val = [];
                $val['value'] = $item['value'];
                $val['key'] = $item['key'];
                return  $val;
            }
        }

        return $value;
    }

    public static function currentValueMultiSelect($value, $key)
    {

        $table = 'olmo_'.self::setLabel($key);
        $items = [];

        if ($value) {
            $ids = explode(',', $value);
            foreach ($ids as $key => $id) {
                $item = [];
                if($table == 'olmo_property'){
                    $data = Db::table('olmo_propertyitem')->where('id', $id)->select('id', 'name_txt_general as value', 'postid_hidden_general')->first();
                    $property = Db::table('olmo_property')->where('id', $data->postid_hidden_general)->select('name_txt_general as property')->first();
                    $item['key'] = @$property->property.' - '.@$data->value;
                } else {
                    $data = Db::table($table)->where('id', $id)->first();
                    $item['key'] = @$data->name_txt_general ?? @$data->username_txt_general ?? @$data->email_email_general ?? '';
                }                
                $item['value'] = (string)@$data->id ?? '';                
                array_push($items, $item);
            }
        }

        return $items;
    }

    public static function normalizestring($text, string $divider = '-')
    {
        $text         = pathinfo($text);
        $filename     = $text['filename'];
        $extension    = $text['extension'];
        $filename     = self::clean($filename);
        $filename     = strtolower($filename);
        return $filename . '.' . $extension;
    }

    public static  function clean($string)
    {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        return preg_replace('/-+/', '-', $string);
    }

    public static function str_replace_first($search, $replace, $subject)
    {
        $search = '/' . preg_quote($search, '/') . '/';
        return preg_replace($search, $replace, $subject, 1);
    }

    public static function isDefaultModel($model)
    {
        if($model == 'property' || $model == 'template' || $model == 'slider' || $model == 'menu' || $model == 'user' || $model == 'visualblock'){
            return true;
        } 
        return false;
    } 
}
