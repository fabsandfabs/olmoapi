<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersFilemanager;

class HelpersVisualBlock
{

    private const CONTAINERS_DIRECTORY_NAME = 'app'; 

    private const CONTAINER_SECTION = 'Settings';    

    public static function retrieveBlocks($ids) {

        $idsArray = $ids == '' ? [] : explode(',', $ids);        
        $data = [];

        $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SECTION . DIRECTORY_SEPARATOR . "VisualBlock");
        $visualPath = $menuDirectory.'/'.'visualblock.json';
        $visualRaw = json_decode(file_get_contents($visualPath), true);        

        foreach($idsArray as $value) {

            $row = DB::table('olmo_visualblock')->where('id', $value)->first();
            $block = [];

            if($row){
                foreach($row as $k=>$v) {

                    $item = [];
                    $item['id']     = $row->id;
                    $item['name']   = (string)$k;
                    $item['label']  = HelpersData::setLabel((string)$k);
                    $type           = HelpersData::getType((string)$k,'');
                    $item['type']   = $type;
                    $item['title']  = $row->name;
    
                    if($type === 'filemanager'){
                        $item['value']  = HelpersFilemanager::getSinglePathFmanagerById($v, 'media');
                        // ApiHelper::getSinglePathFmanagerById($v, 'media');
                    } else {
                        $item['value']  = (string)$v;
                    }
    
                    if($k != 'id' && $k != 'created_at' && $k != 'updated_at'){               
                        foreach($visualRaw as $field){
                            if($row->name == $field['name']){
                                if(in_array(explode('_', $item['name'])[0], $field['blocks'])){
                                    $item['icon']  = $field['icon'];
                                    array_push($block, $item);
                                }
                            }
                        }                     
                    }
    
                }      
                
                array_push($data, $block);
            }

        }       

        return $data;

    }

}