<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_language', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->string('enabled_is_general', 5)->nullable();
            $table->string('code_txt_general', 10)->nullable();
            $table->string('name_txt_general', 255)->nullable();
            $table->string('displayname_txt_general', 255)->nullable();
            $table->string('default_is_general', 5)->nullable();
            $table->string('menu_is_general', 5)->nullable();
            $table->smallInteger('position_ord_general')->nullable();
        });
        
        Schema::create('olmo_analytics', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            $table->increments('id')->unsigned();
            $table->text('gtmheader_txt_analytics')->nullable();
            $table->text('gtmbody_txt_analytics')->nullable();
            $table->text('gtmheaderprod_txt_analytics')->nullable();
            $table->text('gtmbodyprod_txt_analytics')->nullable();
            $table->text('robots_txt_analytics')->nullable();
            $table->text('robotsprod_txt_analytics')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('generals');
    }
}
