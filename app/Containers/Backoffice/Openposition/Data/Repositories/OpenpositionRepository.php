<?php

namespace App\Containers\Backoffice\Openposition\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class OpenpositionRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
