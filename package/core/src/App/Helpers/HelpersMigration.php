<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class HelpersMigration
{

  private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

  private const CONTAINER_SLIDER = 'Slider';

  private const CONTAINER_VISUALBLOCK = 'VisualBlock';

  public static function Slider()
  {

    $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SLIDER . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($menuDirectory);
    $dataslider = null;

    if ($directory) {
      $sliderPath = $menuDirectory . 'slidertable.json';
      $dataslider = json_decode(file_get_contents($sliderPath), true);
    }

    if($dataslider['table']) {

      Schema::create('olmo_slideritem', function (Blueprint $table) use ($dataslider) {
        // Create new table...
        $table->charset = 'utf8mb4';
        $table->collation = 'utf8mb4_unicode_ci';
        $table->id();
        $table->text("locale_hidden_content")->nullable(false);
        $table->text("parentid_hidden_general")->nullable(false);
        $table->text("model_hidden_content")->nullable(false);
        $table->text("postid_hidden_content")->nullable(false);
        $table->text("columnname_hidden_content")->nullable(false);
        $table->text("name_txt_content")->nullable(false);
        $table->text("primary_filemanager_content")->nullable(false);
        foreach ($dataslider['table'] as $item) {
          if (strpos($item, 'enabled') === false) {
            $table->text($item)->nullable(false);
          } else {
            $table->string($item, 5)->nullable(false);
          }
        }
      });
      
    }
  }

  public static function VisualBlock()
  {

    $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_VISUALBLOCK . DIRECTORY_SEPARATOR);
    $directory = File::isDirectory($menuDirectory);
    $dataslider = null;

    if ($directory) {
      $sliderPath = $menuDirectory . 'visualtable.json';
      $dataslider = json_decode(file_get_contents($sliderPath), true);
    }

    if($dataslider['table']) {

      Schema::create('olmo_visualblock', function (Blueprint $table) use ($dataslider) {
        // Create new table...
        $table->charset = 'utf8mb4';
        $table->collation = 'utf8mb4_unicode_ci';
        $table->id();
        $table->text('name')->nullable(false);
        foreach ($dataslider['table'] as $item) {
          if (strpos($item, 'enabled') === false) {
            $table->text($item)->nullable(false);
          } else {
            $table->string($item, 5)->nullable(false);
          }
        }
      });
      
    }
  }  

  public static function Default($table)
  {
    $table->string('enabled_is_general', 5)->nullable(false);
    $table->text('name_txt_general')->nullable(false);
    $table->dateTime('create_read_general')->useCurrent()->nullable(false);
    $table->dateTime('lastmod_read_general')->useCurrentOnUpdate()->nullable(false); 
  }

  public static function Page($table)
  {
    $table->increments('id')->unsigned();
    self::Locale($table);
    $table->string('enabled_is_general', 5)->nullable(false);
    $table->text('name_txt_general')->nullable(false);
    $table->text('slug_txt_general')->nullable(false);
    $table->dateTime('create_read_general')->useCurrent()->nullable();
    $table->dateTime('lastmod_read_general')->useCurrentOnUpdate()->nullable();
    $table->text('template_id_general')->nullable(false);
    $table->text('position_ord_general')->nullable(false);
    $table->string('menu_is_general')->nullable(false);
  }

  public static function Locale($table)
  {
    $table->text('lang_langs_general')->nullable(false);
    $table->text('locale_hidden_general')->nullable(false);
    $table->text('parentid_hidden_general')->nullable(false);
  }

  public static function Seo($table)
  {
    $table->text('metatitle_txt_seo')->nullable(false);
    $table->text('metadesc_txtarea_seo')->nullable(false);
    $table->text('metakw_txt_seo')->nullable(false);
    $table->text('ogtitle_txt_seo')->nullable(false);
    $table->text('ogdesc_txtarea_seo')->nullable(false);
    $table->text('ogimg_img_seo')->nullable(false);
    $table->text('index_select_seo')->nullable(false);
    $table->text('follow_select_seo')->nullable(false);
  }

  public static function Sitemap($table)
  {
    $table->string('disable_is_sitemap', 5)->nullable(false);
    $table->string('ovewrite_is_sitemap', 5)->nullable(false);
    $table->text('frequency_select_sitemap')->nullable(false);
    $table->text('priority_select_sitemap')->nullable(false);
  }

  // Ecommerce Migration to apply in different container
  public static function ECustomer($table)
  {
    $table->text('address_list_general')->nullable(false);
    $table->text('paymentmethod_hidden_general')->nullable(false);
    $table->text('shippingmethod_hidden_general')->nullable(false);
    $table->text('discountcode_hidden_general')->nullable(false);
    $table->text('shippingaddress_hidden_general')->nullable(false);
    $table->text('billingaddress_hidden_general')->nullable(false);
    $table->text('meta_hidden_general');
  }

  public static function EProduct($table)
  {
    $table->unsignedDecimal('price_num_general', $precision = 12, $scale = 2)->nullable(false);
    $table->integer('qty_spin_general')->nullable(false);
    $table->text('sku_txt_general')->nullable(false);
  }
}
