<?php

namespace App\Containers\Backoffice\Whyifi\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class WhyifiRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
