<?php

// Backoffice routes import
require "General/routes.php";
require "Slider/routes.php";
require "Filemanager/routes.php";
require "Template/routes.php";
require "User/routes.php";
require "Property/routes.php";
require "Menu/routes.php";
require "VisualBlock/routes.php";