<?php

namespace Olmo\Core\App\Http\Controller;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Olmo\Core\App\Helpers\HelpersData;

trait StorageController
{

    public static function serializeAllFile()
    {
      $res = [];
      $path = storage_path('app/public/media');
      $res =  self::scanAllDir($path,0);

      return $res;
    }  
    
    public static function scanAllDir($path,$i)
    {
      $result = [];
    
      $files  = preg_grep('/^([^.])/', scandir($path));
    
      foreach($files as $filename){
        $res = self::getFileInfo($filename,$path,$i);
        $result[]  = $res['file'];
        $i+=1;
      }

      return $result;
    }  
    
    public static function getFileInfo($filename,$path,$i)
    {
        $res               = [];
     
        $info              = pathinfo($filename);
        if(array_key_exists('extension',$info))
        {
          $res['id']           = uniqid();
          $meta                = self::getMeta('file',$filename,$path,$info);
          $res['name']         = $meta['name'];
          $res['physicname']   = $filename;
          $res['type']         = 'file';
          $res['extension']    = $info['extension'];
          $res['description']  = $meta;
        }else{
          $res['id']           = uniqid();
          $meta                = self::getMeta('folder',$filename,$path,$info);
          $res['name']         = $filename;
          $res['physicname']   = '';
          $res['type']         = 'folder';
          $res['extension']    = '';
          $other               = $path.'/'.$info['basename'];
          $res['description']  = $meta;
          $i+=1;
          $res['other']        = self::scanAllDir($other,$i);
        }

        return ['file' => $res, 'index' => $i];
    }
    
    public static function getMeta($type,$filename,$path,$info)
    {
        $file            = Db::table('olmo_storage')->where('filename','like','%'.$filename.'%')->first();
        $res = [];
      
        if($type == 'folder'){
          $res['caption']  = '';
          $res['alt']      = '';  
          $res['name']     = $filename;
          $res['idfile']   = self::generatePath($path.'/'.$info['basename']);;
        } else {     
          $truename        = pathinfo(@$file->truename);
          $res['name']     = $truename['filename'];
          $res['idfile']   = @$file->id;
          $res['path']     = @$file->truename;
          $res['caption']  = ( @$file->caption != '' )  ?   @$file->caption : '';
          $res['alt']      = ( @$file->alt     != '' )  ?   @$file->alt : '';
          $res['public']   = ( @$file->public  != '' )  ?   @$file->public : '';
        }
       
        return $res;
    }
    
    public static function generatePath($path)
    {
      $string = substr($path, 0, strpos($path, 'media/'));
      $replace = str_replace($string, '', $path);
      $finalpath = str_replace('media/', '', $replace);
      return $finalpath;
    }   
    
    public function fileEditMethod(Request $request)
    {
        $code             = 200;
        $idfile           = $request->idfile;
        $filename         = $request->filename;
        $file             = Db::table('olmo_storage')->where('id', $idfile)->first();
        $infofile         = pathinfo($file->truename);
        $current_filename = $infofile['filename'];
        $truename         = $file->truename;
        $checkTrueName    = 0;
        $resUpdate       = false;
        $data   = [
            'alt'     => $request->alt,
            'caption' => $request->caption,
            'public'  => $request->public
        ];

        if ($filename != '') {
            $filename  = HelpersData::normalizestring($filename);
            $truename  = str_replace($current_filename, $filename, $truename);
            $data['truename'] = $truename;
            $checkTrueName =  DB::table('olmo_storage')->where('truename', $truename)->where('id', '!=', $idfile)->get()->count();
        }

        if ($checkTrueName > 0) {
            $code = 403;
        } else {
            $resUpdate = Db::table('olmo_storage')->where('id', $idfile)->update($data);
        }


        $msg = [
            'update'   => $resUpdate,
            'infofile' => $infofile,
            'truename' => $truename
        ];

        return response($msg, $code);
    }    
    
    public  static function folderCreateMethod($request)
    {

        $folder        = @$request->folderName;
        $path  		   = 'public/media/'.$folder;
        $res           = Storage::makeDirectory($path,0777,true);
        $public_path   = $folder;

        if($res){
            $res = true;
        }else{
            $res = false;
        }

        $arr['data']         = $public_path;
        $arr['isSuccess']    = $res;
        $arr['errorMessage'] = '';
        return $arr;
    }
    
    /**
     * ---------------------------
     * WORK IN PROGRESS
     * ---------------------------
     */
    public  static function fileMoveMethod()
    {

        $storage_id    = @$_REQUEST['storage_id'];
        $newpath       = @$_REQUEST['storage_id'];

        $path  		   = 'public/media/'.$folder;
        $res           = Storage::makeDirectory($path);
        $public_path   = '/media/'.$folder;

        if($res){
            $res = true;
        }else{
            $res = false;
        }

         DB::table('olmo_storage')->where('id',$storage_id)->update([
           'filename' 	=> $newpath
        ]); 

        $arr['data']         = $public_path;
        $arr['id']           = $storage_id;
        $arr['isSuccess']    = $res;
        $arr['errorMessage'] = null;
        return $arr;
    }    

    public function folderEdit(Request $request)
    {
        $sc               = 200;
        $folderPath       = $request->folderPath;
        $folderName       = $request->folderName;
        $paths            = explode('/', $folderPath);
        $lastDir          = $paths[count($paths) - 1];
        $newpath = '';
        for ($i = 0; $i < count($paths) - 1; $i++) {
            $newpath .= $paths[$i] . '/';
        }
        $newpath .= $folderName;
        $folderPathFull = storage_path('app/public/media/' . $folderPath);
        $newpathFull = storage_path('app/public/media/' . $newpath);
        if (file_exists($folderPathFull)) {
            rename($folderPathFull, $newpathFull);
            $files = Db::table('olmo_storage')->get();
            foreach ($files as $file) {
                $truename    = $file->truename;
                $idfile      = $file->id;
                $newtruename = HelpersData::str_replace_first($folderPath, $newpath, $truename);
                Db::table('olmo_storage')->where('id', $idfile)->update(['truename' => $newtruename]);
            }
            return response("Nome cartella cambiato", 200);
        } else {
            return response("Il percorso ". storage_path('app/public/media/' . $folderPath) ." non esiste", 403);
        }
    }
    
    public function moveFilesMethod(Request $request)
    {
        $sc                = 200;
        $movefiles         = $request->movefiles;
        $destinationfolder = $request->destinationfolder;
        $fileMoved = [];

        foreach ($movefiles as $file) {
            $query                 = Db::table('olmo_storage')->where('id', $file)->first();
            $truename                 = $query->truename;
            $filename                 = $query->filename;
            $path                     = pathinfo($truename);
            $newpath                  = $destinationfolder . '/' . $path['basename'];
            $physicnewpath            = $destinationfolder . '/' . $filename;
            $current_physic_path      = $path['dirname'] . '/' . $filename;
            // $folderPathFull           = storage_path('app/public/media/'. $filename);
            $physicnewpathFull        = storage_path('app/public/media/' . $physicnewpath);
            $current_physic_path_full = storage_path('app/public/media/' . $current_physic_path);

            if (file_exists($current_physic_path_full)) {
                rename($current_physic_path_full, $physicnewpathFull);
                $newtruename = HelpersData::str_replace_first($truename, $newpath, $truename);
                Db::table('olmo_storage')->where('id', $file)->update(['truename' => $newtruename]);
                $message = "File moved. Id: ";
                $code = 200;
                array_push($fileMoved, $file);
            } else {
                $message = "The file is not in the file system";
                $code = 404;
            }
        }

        $message = $message . implode(',', $fileMoved);

        return response($message, 200);
    }   
    
    public function folderEditMethod(Request $request)
    {
        $folderPath       = $request->folderPath;
        $folderName       = $request->folderName;
        $paths            = explode('/', $folderPath);
        $newpath = '';
        for ($i = 0; $i < count($paths) - 1; $i++) {
            $newpath .= $paths[$i] . '/';
        }
        $newpath .= $folderName;
        $folderPathFull = storage_path('app/public/media/' . $folderPath);
        $newpathFull = storage_path('app/public/media/' . $newpath);
        if (file_exists($folderPathFull)) {
            rename($folderPathFull, $newpathFull);
            $files = Db::table('olmo_storage')->get();
            foreach ($files as $file) {
                $truename    = $file->truename;
                $idfile      = $file->id;
                $newtruename = HelpersData::str_replace_first($folderPath, $newpath, $truename);
                Db::table('olmo_storage')->where('id', $idfile)->update(['truename' => $newtruename]);
            }
            return response("Nome cartella cambiato", 200);
        } else {
            return response("Il percorso ". storage_path('app/public/media/' . $folderPath) ." non esiste", 403);
        }
    }   
    
    public function deleteFileMethod(Request $request)
    {
        $ids = $request->ids;

        foreach ($ids as $id) {
            $query = Db::table('olmo_storage')->where('id', $id)->first();

            if ($query) {
                $pathname = $query->truename;
                $physicName = $query->filename;
                $filename = explode("/", $pathname);
                $path = str_replace($filename[count($filename) - 1], $physicName, $pathname);

                $physicFilePath = storage_path('app/public/media/' . $path);

                if (file_exists($physicFilePath)) {
                    if (realpath($physicFilePath)) {
                        if (is_writable($physicFilePath)) {
                            unlink($physicFilePath);
                            Db::table('olmo_storage')->where('id', $id)->delete();
                        } else {
                            return response('error here' . $id, 400);
                        }
                    } else {
                        return response('error here' . $id, 400);
                    }
                    $code = 200;
                    $message = "File deleted";
                } else {
                    return response('error here' . $id, 400);
                }
            } else {
                self::deleteFoldersAndFile(storage_path('app/public/media/' . $id));
                $code = 200;
                $message = "File deleted";
            }
        }

        return response($message, $code);
    }
    
    public static function deleteFoldersAndFile($dir)
    {
        $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator(
            $it,
            \RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                $filename = explode('/', $file);
                Db::table('olmo_storage')->where('filename', $filename[count($filename) - 1])->delete();
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }    

    public static function getFileManagerMethod($request)
    {
        $truename  = $request->any;
        $pathinfo  = pathinfo($truename);
        $dirname   = $pathinfo['dirname'];
        $folder    = 'filemanager';
        $image = Db::table('olmo_storage')->where('truename',$truename)->where('model',$folder)->first();
        
        $valid   = true;
        if($image) {

            if($image->public == "false"){
                if(@$_REQUEST['olmotoken']){

                  $token = $_REQUEST['olmotoken'];

                  $query = Db::table('olmo_tokens')
                            ->where('modelid',$image->truename)
                            ->where('token',$token)                     
                            ->first();                 

                  if(!$query){
                    $valid = false;
                  } else {
                    Db::table('olmo_tokens')
                        ->where('modelid',$image->truename)
                        ->where('token',$token)
                        ->update([
                            'token' => '',
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                  }
                } else {
                    return redirect(404);
                    exit();                    
                }
            }

            if(!$valid)
            {
               return redirect(404);
               exit();
            }

            if($dirname == '')
                $finalpath = $image->filename;
            else
                $finalpath = $dirname.'/'.$image->filename;

            $type    = pathinfo($truename, PATHINFO_EXTENSION);
            $data    = storage_path('app/public/media/'. $finalpath);
            $content = file_get_contents($data);
            $expires = 14 * 60*60*24;
            if($type == 'pdf') {
                $explodefile = explode('/', $image->truename);
                $filename = $explodefile[count($explodefile) - 1];
                return response()->download($data, $filename);
                // alternativa per visualizzazione inline:
                // header("Content-type:application/pdf");
                // header("Content-Disposition:inline;filename=".$image->truename);
                // readfile($data);
            } else {
                if($type == 'mp4'){
                    header("Content-Type: video/".$type);
                } else {
                    header("Content-Type: image/".$type);
                }
            }
            header("Content-Length: " . strlen($content));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $content;
            exit();
        }

        return redirect(404);

    }       

    public static function getFileMethod($request){

        $truename  = $request->truename;
        $folder    = $request->folder;

        $image = Db::table('olmo_storage')->where('truename',$truename)->where('model',$folder)->first();

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
        ];   
        
        if($image->public  == 'false'){
            return response(403);
            exit();
        }
        
        if($image) {
            $type = pathinfo($truename, PATHINFO_EXTENSION);
            $data = storage_path('app/public/'.$categories_files[$type].'/'.$image->model.'/'. $image->filename);
            $content = file_get_contents($data);
            $expires = 14 * 60*60*24;
            if($type == 'pdf') {
                return response()->download($data, $image->truename);
                // alternativa per visualizzazione inline:
                // header("Content-type:application/pdf");
                // header("Content-Disposition:inline;filename=".$image->truename);
                // readfile($data);
            } else {
                header("Content-Type: image/".$type);
            }
            header("Content-Length: " . strlen($content));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $content;
            exit();
        }

        return response(404);
    }

    public static function getBarcodeMethod($request){
            $code    = $request->code;
            $expires   = 14 * 60*60*24;
            $generator = new BarcodeGeneratorPNG();
            $data      =  $generator->getBarcode($code, $generator::TYPE_CODE_128);
            header("Content-Type: image/png");
            header("Content-Length: " . strlen($data));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $data;
            exit();

    }        

}