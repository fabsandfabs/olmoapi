<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilemanagersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_storage', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->string('filename')->nullable();
            $table->string('truename')->nullable();            
            $table->string('model')->nullable();
            $table->string('type')->nullable();
            $table->text('alt')->nullable();
            $table->text('caption')->nullable();
            $table->text('meta')->nullable();
            $table->string('public',5)->nullable();
            $table->timestamps();
            //$table->softDeletes();
        });

        Schema::create('olmo_tokens', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->increments('id')->unsigned();
            $table->string('token')->nullable();
            $table->string('model')->nullable();
            $table->string('modelid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_storage');
        Schema::dropIfExists('olmo_tokens');
    }
}
