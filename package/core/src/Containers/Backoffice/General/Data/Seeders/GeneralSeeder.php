<?php

namespace Olmo\Core\Containers\Backoffice\General\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralSeeder extends Seeder
{
    public function run()
    {

        DB::table('olmo_language')->truncate();
       
        DB::table('olmo_language')->insert([
            'enabled_is_general'        => 'true',
            'code_txt_general'          => 'en',
            'name_txt_general'          => 'english',
            'displayname_txt_general'   => 'English',
            'default_is_general'        => 'true',
            'position_ord_general'      => '0'
        ]);	
       
        DB::table('olmo_language')->insert([
            'enabled_is_general'        => 'false',
            'code_txt_general'          => 'it',
            'name_txt_general'          => 'italian',
            'displayname_txt_general'   => 'Italiano',
            'default_is_general'        => 'false',
            'position_ord_general'      => '1'
        ]);	
        	

        DB::table('olmo_analytics')->truncate();
       
        DB::table('olmo_analytics')->insert([
            'gtmheader_txt_analytics'       => '',
            'gtmbody_txt_analytics'         => '',
            'gtmheaderprod_txt_analytics'   => '',
            'gtmbodyprod_txt_analytics'     => '',
            'robots_txt_analytics'          => '',
            'robotsprod_txt_analytics'      => ''
        ]);        

    }
}
