<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Helpers\HelpersData;

class HelpersLang
{
    public static function getLangsBymodelId($table, $id)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */

        $langs = self::validLang();
        $res   = [];
        $i     = 0;

        $default_id  = self::getDefaultLangPostId($table, $id);
        $default_lang = self::getDefaultlang();

        foreach ($langs as $lang) {

            $exist = false;
            $id = $default_id;

            if ($default_lang == $lang) {
                $exist = true;
            } else {
                $check =  Db::table($table)->where('parentid_hidden_general', $default_id)->where('locale_hidden_general', $lang)->first();
                if ($check) {
                    $exist = true;
                    $id    = $check->id;
                }
            }

            $res[$i]['code']  = $lang;
            $res[$i]['exist'] = $exist;
            $res[$i]['id']    = (string)$id;
            $i++;
        }

        return $res;
    }

    public static function validLang()
    {
        $data =  Db::table('olmo_language')->where('enabled_is_general', 'true')->select('code_txt_general')->get()->toArray();
        $response = [];
        foreach ($data as $d) {
            $response[$d->code_txt_general] = $d->code_txt_general;
        }

        return $response;
    }

    public static function checkIfvalidLang($lang)
    {
        $langs = self::validLang();
        return in_array($lang, $langs);
    }

    public static function getDefaultLang()
    {
        $default = Db::table('olmo_language')->where('default_is_general', 'true')->first();
        return $default->code_txt_general;
    }

    public static function checkIfTranslable($model)
    {
        if (HelpersData::isDefaultModel($model)) {
            $attribute = ('Olmo\Core\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model))::theAttributes();
        } else {
            $attribute = ('App\Containers\Backoffice\\' . ucfirst($model) . '\\Models\\' . ucfirst($model))::theAttributes();
        }
        return $attribute['lang'];
    }

    public static function getDefaultLangPostId($table, $id)
    {
        $data = DB::table($table)->where('id', $id)->first();
        $default_id = $data->parentid_hidden_general ? $data->parentid_hidden_general : $id;
        return $default_id;
    }

    /**
     * 
     * THIS IS DEPRECATED
     * 
     * Here you go!
     * Copy the default post values inside the current translated post
     * 
     * NEED TO DO
     * Copy correct relation on select and multiselect, what happen when there is no post created for that lang?
     */
    public static function copyDefautLang($request, $attribute)
    {
        $dataPost = HelpersData::idLangPost($request);

        $default_id = Db::table($attribute['table'])->where('id', $dataPost['id'])->first()->parentid_hidden_general;
        $default = Db::table($attribute['table'])->where('id', $default_id)->first();

        unset($default->id);
        unset($default->lang_langs_general);
        unset($default->locale_hidden_general);
        unset($default->parentid_hidden_general);
        unset($default->lastmod_read_general);

        $default = (array)$default;

        $updatePost = Db::table($attribute['table'])->where('id', $dataPost['id'])->update($default);
    }

    /**
     * Check if need copy-from button
     * This happen when a user creates a localized post, the button will be always
     * then print a button to copy the content from the default post
     */
    public static function addCopyDefaultFrom($data, $id)
    {
        $data = (array)$data;
        if (isset($data['parentid_hidden_general'])) {
            if (!$data['parentid_hidden_general'] == "") {
                $data = ["copytranslate" => $id] + $data;
            }
        }
        return $data;
    }

    /**
     * Create a translation of the requested slide copying everithing but the fields
     * who declare the post has translation of:
     * 
     * locale_hidden_content
     * parentid_hidden_content
     * postid_hidden_content     
     */
    public static function generateTranslatedRelation($id, $lang, $postid, $table)
    {
        $getSlide = Db::table($table)->where('id', $id)->first();
        $slide = (array)$getSlide;

        $slide['locale_hidden_content'] = $lang;
        $slide['parentid_hidden_content'] = $id;
        $slide['postid_hidden_content'] = $postid;
        unset($slide['id']);

        $insertSlide = Db::table($table)->insertGetId($slide);
        return $insertSlide;
    }

    /**
     * THIS METHOD MUST BE DELETED AND MERGED WITH THE ONE ABOVE BUT THE 
     * CHANGE IS REALLY BREAKING, THE PROPITEM COLUMN NAME MUST BE CONVERTED TO _CONTENT INSTEAD _GEENRAL 
     * 
     * Create a translation of the requested slide copying everithing but the fields
     * who declare the post has translation of:
     * 
     * locale_hidden_content
     * parentid_hidden_content
     * postid_hidden_content     
     */
    public static function generateTranslatedPropVal($id, $lang, $postid, $table)
    {
        $getSlide = Db::table($table)->where('id', $id)->first();
        $slide = (array)$getSlide;

        $slide['locale_hidden_general'] = $lang;
        $slide['parentid_hidden_general'] = $id;
        $slide['postid_hidden_general'] = $postid;
        unset($slide['id']);

        $insertSlide = Db::table($table)->insertGetId($slide);
        return $insertSlide;
    }

    /**
     * Three options
     * $data -> the array of fileds the method has to translates
     * $lang -> the lang of the request
     * $postid -> the request post id
     * 
     * Basically the method takes care to recreate the relation 
     * between the original post and its translation in the request language
     * 
     * Big deal bro
     */
    public static function translateRelations($data, $lang, $postid)
    {
        $toArray = (array)$data;
        foreach ($toArray as $key => $item) {
            $type = HelpersData::getType($key);
            $label = HelpersData::setLabel($key);
            if ($type == 'select' || $type == 'multiselect') {
                $table = 'olmo_' . $label;
                $checkTable = Schema::hasTable($table);
                if ($checkTable) {
                    if ($type == 'select') {
                        $translable = HelpersLang::checkIfTranslable($label);
                        if ($translable) {
                            $getPost = Db::table($table)->where('parentid_hidden_general', $item)->where('locale_hidden_general', $lang)->first();
                            $toArray[$key] = isset($getPost->id) ? (string)$getPost->id : '';
                        }
                    } else if ($type == 'multiselect') {
                        $translable = HelpersLang::checkIfTranslable($label);
                        if ($translable) {
                            $ids = explode(',', $item);
                            foreach ($ids as $k => $id) {
                                $getPost = Db::table($table)->where('parentid_hidden_general', $id)->where('locale_hidden_general', $lang)->first();
                                if ($k == 0) {
                                    $toArray[$key] = isset($getPost->id) ? (string)$getPost->id : '';
                                } else {
                                    $toArray[$key] = isset($getPost->id) ? $toArray[$key] . ',' . (string)$getPost->id : $toArray[$key];
                                }
                            }
                        }
                    }
                }
            } else if ($type == 'slider' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getPropValue = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_slideritem');
                    if ($k == 0) {
                        $toArray[$key] = isset($getSlideId) ? (string)$getSlideId : '';
                    } else {
                        $toArray[$key] = isset($getSlideId) ? $toArray[$key] . ',' . (string)$getSlideId : $toArray[$key];
                    }
                }
            } else if ($type == 'property' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getPropValue = HelpersLang::generateTranslatedPropVal($id, $lang, $postid, 'olmo_propertyitem');
                    if ($k == 0) {
                        $toArray[$key] = isset($getPropValue) ? (string)$getPropValue : '';
                    } else {
                        $toArray[$key] = isset($getPropValue) ? $toArray[$key] . ',' . (string)$getPropValue : $toArray[$key];
                    }
                }
            } else if ($type == 'visual' && $item != '') {
                $ids = explode(',', $item);
                foreach ($ids as $k => $id) {
                    $getPropValue = HelpersLang::generateTranslatedRelation($id, $lang, $postid, 'olmo_visualblock');
                    if ($k == 0) {
                        $toArray[$key] = isset($getPropValue) ? (string)$getPropValue : '';
                    } else {
                        $toArray[$key] = isset($getPropValue) ? $toArray[$key] . ',' . (string)$getPropValue : $toArray[$key];
                    }
                }
            }
        }

        return $toArray;
    }

    public static function ArrayValidLanguage(){

        $langs = Db::table('olmo_language')->where('enabled_is_general','true')->get();
        $validlang = [];

        foreach($langs as $lang){
            array_push($validlang, $lang->code_txt_general);
        }

        return $validlang;

    } 

}
