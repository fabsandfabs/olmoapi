<?php

namespace Olmo\Core\Containers\Backoffice\General\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class General extends OlmoMainModel
{

    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'General';

    public $table = 'olmo_language';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_language',
            'required' => [],
            'listing' => 'code_txt_general, displayname_txt_general, default_is_general, enabled_is_general, id',
            'requiredbackoffice' => [
                'code_txt_general',
                'displayname_txt_general',
                'name_txt_general'
            ],
            'rules' => [
                [
                    'type' => 'default',
                    'field' => 'default_is_general',
                    'table' => 'olmo_language',
                    'lang' => false,
                    'errortxt' => 'Just one default language can exists'
                ],
                [
                    'type' => 'ifthen',
                    'field_one' => 'default_is_general',
                    'condition_one' => 'true',
                    'field_two' => 'enabled_is_general',                    
                    'condition_two' => 'true',
                    'table' => 'olmo_language',
                    'lang' => false,
                    'errortxt' => "Can't be default if not enabled"
                ]                
            ],                       
        ];
    }    
}
