<?php

namespace App\Containers\Backoffice\Selectedby\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class SelectedbyRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
