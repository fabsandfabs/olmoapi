# Slider setup guide

###### This is a basic example of how could be a slider table. This setup is laso the one you find inside the table key
```
[
    "secondary_filemanager_content",
    "pretitle_editor_content",
    "title_editor_content",
    "subtitle_editor_content",
    "cta_txt_content",
    "link_txt_content"
]
```

###### Some of them are mandatory, you haven to do nothing these are included by default
```
    "locale_hidden_content",
    "parentid_hidden_content",
    "model_hidden_content",
    "postid_hidden_content",
    "columnname_hidden_content",
    "name_txt_content",
    "primary_filemanager_content",
```

###### The structure of the json can't be changed, table and default are madatory then you can add the key you want with the name of the model, it must be an array type inside every object there must be two keys *slider* type string and *field* type array the *field* key is a list of the field you whant to display
```
{
    "page": [
        {
            "slider": "...",
            "field": [...]
        }
    ]
}
```