<?php

namespace Olmo\Core\Containers\Backoffice\User\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class User extends OlmoMainModel
{

    public $timestamps = false;
    
    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'User';

    public $table = 'olmo_user';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_user', 
            'required' => [],
            'requiredbackoffice' => [
                'name_txt_general',
                'email_email_general',
                'role_id_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'email_email_general',
                    'table' => 'olmo_user',
                    'lang' => false,
                    'errortxt' => 'Email duplicated'
                ],
                [
                    'type' => 'equal',
                    'field_one' => 'password_pwd_general',
                    'field_two' => 'confirmpassword_pwd_general',
                    'errortxt' => 'Password not match'
                ]                
            ]
        ];
    }    
}
