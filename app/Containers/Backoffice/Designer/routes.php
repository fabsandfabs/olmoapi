<?php

use App\Containers\Backoffice\Designer\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/designer', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the designer post
Route::get('{lang}/designer/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the designer post
Route::put('{lang}/designer/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the designer post
Route::post('{lang}/designer/create', [Controller::class, 'createPost'])->middleware(['auth:api']);