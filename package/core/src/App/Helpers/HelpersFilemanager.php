<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelpersFilemanager
{

    public static function getSinglePathFmanagerById($id, $media = ''){

        $im = DB::table('olmo_storage')->where('id',$id)->first();
        $media = $media === '' ? $media : '/'.$media.'/';
        if($im){
            return $media.$im->truename;
        }else{
            return '';
        }
    } 

    public static function getSinglePathStorageById($id){

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
          ];

        $im = DB::table('olmo_storage')->where('id',$id)->first();
        if($im){
             return '/'.$categories_files[$im->type].'/'.$im->model.'/'.$im->truename;
        }else{
         return '';
        }
    }    

}