<?php

namespace App\Containers\Backoffice\Technology\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class TechnologyRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
