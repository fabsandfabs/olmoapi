<?php

namespace App\Containers\Backoffice\Sector\Controllers;

use Illuminate\Http\Request;

use App\Containers\Backoffice\Sector\Models\Sector;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Sector::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Sector::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Sector::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function updatePost(Request $request)
    {
    	$attribute = Sector::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }       

}
