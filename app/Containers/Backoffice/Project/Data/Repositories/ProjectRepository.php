<?php

namespace App\Containers\Backoffice\Project\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class ProjectRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
