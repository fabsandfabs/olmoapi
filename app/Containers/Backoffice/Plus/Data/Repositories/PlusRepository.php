<?php

namespace App\Containers\Backoffice\Plus\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class PlusRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
