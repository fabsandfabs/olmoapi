<?php

use App\Containers\Backoffice\Page\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/page', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the page post
Route::get('{lang}/page/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the page post
Route::put('{lang}/page/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the page post
Route::post('{lang}/page/create', [Controller::class, 'createPost'])->middleware(['auth:api']);