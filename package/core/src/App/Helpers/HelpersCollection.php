<?php

namespace Olmo\Core\App\Helpers;

use Olmo\Core\Loaders\OlmoLoadersContainers;

class HelpersCollection
{

    public static function get($type)
    {
        /**
         * PLEASE FIX ME OR AT LEAST CHECK ME OUT
         */
        $res = [];

        $collection['customerprop'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'vat', 'value' => 'vat']
        ];

        $collection['customercondition'] = [
            ['key' => '', 'value' => ''],
            ['key' => '=', 'value' => '='],
            ['key' => '=', 'value' => '!='],
            ['key' => '>', 'value' => '>'],
            ['key' => '<', 'value' => '<'],
            ['key' => '>=', 'value' => '>='],
            ['key' => '<=', 'value' => '<=']
        ];

        $collection['ordercondition'] = [
            ['key' => '', 'value' => ''],
            ['key' => '=', 'value' => '='],
            ['key' => '=', 'value' => '!='],
            ['key' => '>', 'value' => '>'],
            ['key' => '<', 'value' => '<'],
            ['key' => '>=', 'value' => '>='],
            ['key' => '<=', 'value' => '<=']
        ];

        $collection['ordernumber'] = [
            ['key' => '', 'value' => '']
        ];

        $collection['model'] = [
            ['key' => '',          'value' => ''],
            ['key' => 'page',      'value' => 'page'],
            ['key' => 'product',   'value' => 'product'],
            ['key' => 'arredo',  'value' => 'arredo'],
            ['key' => 'blog',      'value' => 'blog'],
            ['key' => 'sector',   'value' => 'sector'],
            ['key' => 'project',   'value' => 'project'],
            ['key' => 'whyifi',  'value' => 'whyifi'],
            ['key' => 'technology',  'value' => 'technology'],
            ['key' => 'formatpers',  'value' => 'formatpers']
        ];

        $collection['index'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'index', 'value' => 'index'],
            ['key' => 'noindex', 'value' => 'noindex'],
        ];

        $collection['follow'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'follow', 'value' => 'follow'],
            ['key' => 'nofollow', 'value' => 'nofollow']
        ];

        $collection['frequency'] = [
            ['key' => '',        'value' => ''],
            ['key' => 'always',  'value' => 'always'],
            ['key' => 'hourly',  'value' => 'hourly'],
            ['key' => 'daily',   'value' => 'daily'],
            ['key' => 'weekly',  'value' => 'weekly'],
            ['key' => 'monthly', 'value' => 'monthly'],
            ['key' => 'never',   'value' => 'never']
        ];

        $collection['priority'] = [
            ['key' => '', 'value' => ''],
            ['key' => '0', 'value' => '0'],
            ['key' => '0.1', 'value' => '0.1'],
            ['key' => '0.2', 'value' => '0.2'],
            ['key' => '0.3', 'value' => '0.3'],
            ['key' => '0.4', 'value' => '0.4'],
            ['key' => '0.5', 'value' => '0.5'],
            ['key' => '0.6', 'value' => '0.6'],
            ['key' => '0.7', 'value' => '0.7'],
            ['key' => '0.8', 'value' => '0.8'],
            ['key' => '0.9', 'value' => '0.9'],
            ['key' => '1', 'value' => '1'],
        ];

        $collection['formtype'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'login', 'value' => 'login'],
            ['key' => 'register', 'value' => 'register'],
            ['key' => 'resetpassword', 'value' => 'resetpassword'],
            ['key' => 'neworder', 'value' => 'neworder'],
            ['key' => 'orderconfirmed', 'value' => 'orderconfirmed'],
            ['key' => 'ordershipped', 'value' => 'ordershipped'],
            ['key' => 'orderrefunded', 'value' => 'orderrefunded'],
            ['key' => 'customer creation', 'value' => 'customercreation'],
            ['key' => 'customer not active', 'value' => 'customernotactive'],
            ['key' => 'customer active', 'value' => 'customeractive'],
            ['key' => 'customer disabled', 'value' => 'customerdisabled'],
            ['key' => 'customer blocked', 'value' => 'customerblocked'],
            ['key' => 'customer password recovery', 'value' => 'customerpasswordrecovery'],
            ['key' => 'customer password changed', 'value' => 'customerpasswordchanged'],
            ['key' => 'request asstet', 'value' => 'requestasset']
        ];

        $collection['typeaddress'] = [
            ['key' => '', 'value' => ''],
            ['key' => 'shipping', 'value' => 'shipping'],
            ['key' => 'billing', 'value' => 'billing']
        ];

        $collection['customerstatus'] = [
            ['key' => 'Not active', 'value' => 'notactive'],
            ['key' => 'Active', 'value' => 'active'],
            ['key' => 'Disabled', 'value' => 'disabled'],
            ['key' => 'Blocked', 'value' => 'blocked']
        ];

        $collection['orderstatus'] = [
            ['key' => 'Pending', 'value' => 'pending'],
            ['key' => 'Paid', 'value' => 'paid'],
            ['key' => 'Shipped', 'value' => 'shipped'],
            ['key' => 'Completed', 'value' => 'completed'],
            ['key' => 'Canceled', 'value' => 'canceled'],
            ['key' => 'Refunded', 'value' => 'refunded']
        ];

        if($type == 'model'){
            return self::getTemplates();
        } else if(isset($collection[$type])) {
            $res = $collection[$type];
        }

        return $res;
    }    

    public static function getTemplates()
    {

        $containers = OlmoLoadersContainers::getAllContainerPaths();
        $model = [];

        $empty = array('key' => '', 'value' => '');
        array_push($model, $empty);

        foreach($containers as $item){
            $container = last(explode('/', $item));
            $value = [];
            $value['key'] = strtolower($container);
            $value['value'] = strtolower($container);
            array_push($model, $value);
        }

        return $model;

    }

}