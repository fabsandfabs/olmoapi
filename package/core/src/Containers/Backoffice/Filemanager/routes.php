<?php

use Olmo\Core\Containers\Backoffice\Filemanager\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('filemanager/get/all', [Controller::class, 'getAllFiles'])->middleware(['auth:api']);

// Uploader
Route::post('filemanager/uploadchunks', [Controller::class, 'uploadChunk'])->middleware(['auth:api']);
Route::post('filemanager/uploadcomplete', [Controller::class, 'uploadComplete'])->middleware(['auth:api']);

//Create folder
Route::put('filemanager/folder/create', [Controller::class, 'folderCreate'])->middleware(['auth:api']);

//Move file
Route::patch('filemanager/file/move', [Controller::class, 'fileMove'])->middleware(['auth:api']);

//Edit file
Route::patch('filemanager/file/edit/{idfile}', [Controller::class, 'fileEdit'])->middleware(['auth:api']);

//Move file
Route::patch('filemanager/files/move', [Controller::class, 'moveFiles'])->middleware(['auth:api']);

//Move folder
Route::patch('filemanager/folder/move', [Controller::class, 'moveFolder']);

//Edit folder
Route::patch('filemanager/folder/edit', [Controller::class, 'folderEdit'])->middleware(['auth:api']);

//Delete
Route::patch('filemanager/delete', [Controller::class, 'deleteFile'])->middleware(['auth:api']);

//Post
Route::post('filemanager/file/token', [Controller::class, 'getToken']);

// Upload single file or multiple files not serchable
Route::post('{lang}/{pagename}/{id}/uploadchunks', [Controller::class, 'uploadChunks'])->middleware(['auth:api']);
Route::post('{lang}/{pagename}/{id}/uploadcomplete', [Controller::class, 'completeChunk'])->middleware(['auth:api']);