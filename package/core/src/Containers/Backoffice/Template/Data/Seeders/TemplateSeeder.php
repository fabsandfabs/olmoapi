<?php

namespace Olmo\Core\Containers\Backoffice\Template\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplateSeeder extends Seeder
{
    public function run()
    {

		DB::table('olmo_template')->insert([
			'name_txt_general'      => 'home',
			'enabled_is_general'    => 'true',
			'locale_hidden_general' => 'en',
			'model_select_general'  => 'page'
		]);

		DB::table('olmo_template')->insert([
			'name_txt_general'      => 'page',
			'enabled_is_general'    => 'true',
			'locale_hidden_general' => 'en',
			'model_select_general'  => 'page'
		]);        
		DB::table('olmo_template')->insert([
			'name_txt_general'      => 'checkoutdetails',
			'enabled_is_general'    => 'true',
			'slug_txt_general'      => 'shipping',
			'locale_hidden_general' => 'en',
			'model_select_general'  => 'page'
		]);

		DB::table('olmo_template')->insert([
			'name_txt_general'      => 'checkoutpayment',
			'enabled_is_general'    => 'true',
			'slug_txt_general'      => 'payment',
			'locale_hidden_general' => 'en',
			'model_select_general'  => 'page'
		]);
		
		DB::table('olmo_template')->insert([
			'name_txt_general'      => 'checkoutsummary',
			'enabled_is_general'    => 'true',
			'slug_txt_general'      => 'summary',
			'locale_hidden_general' => 'en',
			'model_select_general'  => 'page'
		]);    

		DB::table('olmo_template')->insert([
			'name_txt_general'      => 'checkoutcompleted',
			'enabled_is_general'    => 'true',
			'slug_txt_general'      => 'order-completed',
			'locale_hidden_general' => 'en',
			'model_select_general'  => 'page'
		]);

        // ...
        // DB::table('olmo_model')->truncate();
       
		// DB::table('olmo_model')->insert([
		// 	'name_txt_general'      => 'page'
		// ]);

		// DB::table('olmo_model')->insert([
		// 	'name_txt_general'      => 'product'
		// ]);

		// DB::table('olmo_model')->insert([
		// 	'name_txt_general'      => 'category'
		// ]);

		// DB::table('olmo_model')->insert([
		// 	'name_txt_general'      => 'blog'
		// ]);

        // DB::table('olmo_model')->insert([
		// 	'name_txt_general'      => 'casehistory'
		// ]);

        // DB::table('olmo_model')->insert([
		// 	'name_txt_general'      => 'service'
		// ]);         

    }
}
