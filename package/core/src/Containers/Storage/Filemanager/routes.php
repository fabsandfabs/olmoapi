<?php

use Olmo\Core\Containers\Storage\Filemanager\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('media/{any}', [Controller::class, 'getFileManager'])->where('any', '.*');

Route::get('{path}/{folder}/{truename}', [Controller::class, 'getFile']);

Route::get('barcode/{code}', [Controller::class, 'getBarcode']);