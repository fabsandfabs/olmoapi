<?php

use App\Containers\Backoffice\Project\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/project', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the project post
Route::get('{lang}/project/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the project post
Route::put('{lang}/project/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the project post
Route::post('{lang}/project/create', [Controller::class, 'createPost'])->middleware(['auth:api']);