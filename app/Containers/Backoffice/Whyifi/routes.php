<?php

use App\Containers\Backoffice\Whyifi\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/whyifi', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the whyifi post
Route::get('{lang}/whyifi/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the whyifi post
Route::put('{lang}/whyifi/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the whyifi post
Route::post('{lang}/whyifi/create', [Controller::class, 'createPost'])->middleware(['auth:api']);