<?php

use App\Containers\Backoffice\Plus\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/plus', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the plus post
Route::get('{lang}/plus/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the plus post
Route::put('{lang}/plus/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the plus post
Route::post('{lang}/plus/create', [Controller::class, 'createPost'])->middleware(['auth:api']);