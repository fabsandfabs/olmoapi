<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;

class HelpersRulesField
{

    // This is the pattarn to use to create rules
    //
    // ----- Unique
    // [
    //     'type' => 'unique',
    //     'field' => 'slug_txt_general',
    //     'table' => 'olmo_service',
    //     'lang' => true, default: true
    //     'errortxt' => 'Slug duplicate'
    // ] 
    // ----- Equal
    // [
    //     'type' => 'unique',
    //     'field_one' => 'password_pwd_general',
    //     'field_two' => 'confirmpassword_pwd_general',
    //     'errortxt' => 'Password not match'
    // ] 
    // ----- Check slug length
    // ----- Check slug length


    public static function checkRules($request, $attribute)
    {

        $errors = [];        

        $rules = [];
        if(isset($attribute['rules'])){
            $rules = $attribute['rules'];
        }        
    
        foreach($rules as $rule){

            if($rule['type'] == 'equal') {
                if(self::checkEqual($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }
            } else if($rule['type'] == 'unique') {
                if(self::checkUnique($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }
            } else if($rule['type'] == 'default') {
                if(self::checkDefault($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }
            } else if($rule['type'] == 'ifthen') {
                if(self::checkIfThen($request, $rule)){
                    array_push($errors, $rule['errortxt']);
                }                
            }            

        }

        return $errors;
       
    }

    public static function checkIfThen($request, $rule)
    {        
        $filed_one_request = $request->input($rule['field_one']);
        $filed_one_requestString = strval($filed_one_request);

        $filed_two_request = $request->input($rule['field_two']);
        $filed_two_requestString = strval($filed_two_request);

        if($filed_one_request == $rule['condition_one']){
            if($filed_two_request != $rule['condition_two']){
                return $rule['errortxt'];
            }
        }
    }

    public static function checkDefault($request, $rule)
    {
        $id = $request->input('id');
        // $request = json_decode(json_encode($request), true);
        $default = $request[$rule['field']];

        if($default){

            /** Check if a different post has been already set as default */
            $check = Db::table($rule['table'])->where('id', '!=', $id)->where($rule['field'], 'true')->get()->count();
            if($check){
                return $rule['errortxt'];
            }

        }
    }

    public static function checkEqual($request, $rule)
    {

        $a = $request->input($rule['field_one']);
        $b = $request->input($rule['field_two']);

        if($a != $b){
            return $rule['errortxt'];
        }
        
    }

    public static function checkUnique($request, $rule)
    {

        $isLang = false;
        $lang = $request->lang;
        $field = $request->input($rule['field']);
        
        /** Check if the lang is set */
        if(isset($rule['lang'])){
            $isLang = $rule['lang'];
        }

        /** If the lang is set check the post in the same language if not to all */
        if($isLang){
            $check = Db::table($rule['table'])->where($rule['field'],$field)->where('locale_hidden_general', $lang)->where('id','!=',$request->id)->get()->count();
        } else {
            $check = Db::table($rule['table'])->where($rule['field'],$field)->where('id','!=',$request->id)->get()->count();                    
        } 
                        
        /** If the query result is different than 0 print the error */
        if($check > 0){
            return $rule['errortxt'];
        }
        
    }    

}
