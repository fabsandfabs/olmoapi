<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\Menu\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| The Backoffice Menu - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('menu', [Controller::class, 'getMenu'])->middleware(['auth:api']);