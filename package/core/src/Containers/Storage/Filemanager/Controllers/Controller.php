<?php

namespace Olmo\Core\Containers\Storage\Filemanager\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Http\Controller\StorageController;

class Controller extends BackController
{

    use StorageController;

    public function getFileManager(Request $request){

        return $this->getFileManagerMethod($request);

    }
    
    public function getFile(Request $request){

        return $this->getFileMethod($request);
        
    }

    public function getBarcode(Request $request){

        return $this->getBarcodeMethod($request);

    }

}
