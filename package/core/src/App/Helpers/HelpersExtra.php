<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersLang;

class HelpersExtra
{
    public static function managerExtra($request, $attribute)
    {
        if(isset($request->copytranslate)){
            return HelpersLang::copyDefautLang($request, $attribute);
        }        

    }

    public static function theTable($request)
    {
        $table = 'olmo_'.$request->table;
        $columns = Schema::getColumnListing($table);

        $items = [];

        foreach($columns as $item){
            if(
                $item != "id" && $item != "lang_langs_general" && 
                $item != "locale_hidden_general" && 
                $item != "parentid_hidden_general" && 
                $item != "create_read_general" && 
                $item != "lastmod_read_general"
            ){
                $column = [];
                $column['value'] = $item;
                $column['key'] = HelpersData::setLabel($item);
                array_push($items, $column);
            }
        }

        return $items;
    }

    public static function copyColumnsFromDefault($request)
    {
        $table = 'olmo_'.$request->table;
        $lang = $request->lang;
        $id = $request->id;

        $body = $request->toArray();
        $columns = [];

        /**
         * Extract just the true value from the request body
         * basically copy just what true in the request
         */
        foreach($body as $key=>$item){
            if($item){
                array_push($columns, $key);
            }
        }

        /** Get the id of the default post from the current post */
        $idDefault = Db::table($table)->where('id', $id)->select('parentid_hidden_general')->first();
        /** Get the field postto copy from the default post */
        $response = Db::table($table)->where('id', $idDefault->parentid_hidden_general)->select(DB::raw(implode(',', $columns)))->first();

        /** Generate the relation creating new post */
        $response = HelpersLang::translateRelations($response, $lang, $id);
        
        $update = Db::table($table)->where('id', $id)->update((array)$response);
        $data = Db::table($table)->where('id', $id)->first();

        /** Check if the id model to copy from has the lang true, otherwise skip and get to value straight forward */        
        if(HelpersData::isDefaultModel($request->table)){
            $attribute = ('Olmo\Core\Containers\Backoffice\\'.ucfirst($request->table).'\\Models\\'.ucfirst($request->table))::theAttributes();
        } else {
            $attribute = ('App\Containers\Backoffice\\'.ucfirst($request->table).'\\Models\\'.ucfirst($request->table))::theAttributes();
        }        

        $data = HelpersLang::addCopyDefaultFrom($data, $id);
        $thepost = BackController::serializePost($data, $attribute, $lang, $id);

        return response($thepost, 200);
    }   
    
}