<?php

namespace Olmo\Core\App\Helpers;

use Olmo\Core\App\Helpers\HelpersData;

class HelpersRequiredField
{

    public static function checkRequired($request, $attribute)
    {
        $errors = [];

        /** Convert the request from object in to array */
        $requestBody = $request->toArray();
        /** Extract just the keys from the array */
        $keysRequest = array_keys($requestBody);

        $required = [];
        /** 
         * First check if the request come from the Model Property wich is the only one with requiredbackofficevalue
         * then move on
         * 
         * */
        if(isset($attribute['requiredbackofficevalue'])){
            $required = $attribute['requiredbackofficevalue'];
        } else if(isset($attribute['requiredbackoffice'])){
            $required = $attribute['requiredbackoffice'];
        }

        foreach($required as $field){

            if(self::missingField($field, $requestBody)){
                array_push($errors, self::missingField($field, $requestBody));
            }

            if(in_array($field, $keysRequest)){
                if($field == 'email_email_general'){
                    $email = self::checkIfisEmail($requestBody['email_email_general']);
                    if(!$email){
                        if($requestBody['email_email_general'] == ''){
                            $reply = 'Email';
                        } else {
                            $reply = $requestBody['email_email_general'];
                        }
                        array_push($errors, $reply . " is not an email");
                    }
                }
            }

        }
        
        /** Remove null values from array */
        $errors = array_filter($errors, fn($value) => !is_null($value));
        
        return $errors;
        
    }

    public static function missingField($field, $requestBody)
    {
        $type = HelpersData::getType($field);
        if($type == 'select'){
            if($requestBody[$field]['value'] == ""){
                return ucfirst(HelpersData::setLabel($field)) . " missing";
            }
        }
        if($requestBody[$field] == ""){
            return ucfirst(HelpersData::setLabel($field)) . " missing";
        } 
        
        return null;
    }

    public static function checkIfisEmail($email) {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false && $find2 > $find1);
     }    

}