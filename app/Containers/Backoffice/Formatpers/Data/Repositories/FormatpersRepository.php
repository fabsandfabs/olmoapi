<?php

namespace App\Containers\Backoffice\Formatpers\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class FormatpersRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
