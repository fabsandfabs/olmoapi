<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HelpersUser
{

    public static function checkToken()
    {
        $header = getallheaders();
        $token = isset($header['x-token']);
        $bigToken = isset($header['X-Token']);

        if($token){
            $token = $header['x-token'];
            return $token;
        } 

        if($bigToken){
            $bigToken = $header['X-Token'];
            return $bigToken;
        }

        return abort(403);
    }

    public static function checkUserToken(Request $request)
    {
        $token = self::checkToken();
        if($token){
            $user = DB::table('olmo_user')->where('token_hidden_general',$token)->get();
            if(count($user) == 0){
                abort(403);
            }            
        } else {
            abort(403);            
        }
    }

    public static function getUser()
    {
        $token = self::checkToken();
        if($token){
            $user = DB::table('olmo_user')->where('token_hidden_general', $token)->first();
            if($user){
                return $user;
            }
            return abort(403);
        }
    }    

    public static function usersByRole()
    {
        $user = self::getUser();
        $role = $user->role_id_general;

        return Db::table('olmo_user')
                ->select(
                    'email_email_general', 
                    'enabled_is_general', 
                    'role_id_general', 
                    'id'
                )->where('role_id_general', '>=', $role)->get();
    }

}