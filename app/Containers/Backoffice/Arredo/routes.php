<?php

use App\Containers\Backoffice\Arredo\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/arredo', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the arredo post
Route::get('{lang}/arredo/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the arredo post
Route::put('{lang}/arredo/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the arredo post
Route::post('{lang}/arredo/create', [Controller::class, 'createPost'])->middleware(['auth:api']);