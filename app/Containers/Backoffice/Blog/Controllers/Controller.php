<?php

namespace App\Containers\Backoffice\Blog\Controllers;

use Illuminate\Http\Request;

use App\Containers\Backoffice\Blog\Models\Blog;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Blog::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Blog::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Blog::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function updatePost(Request $request)
    {
    	$attribute = Blog::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }       

}
