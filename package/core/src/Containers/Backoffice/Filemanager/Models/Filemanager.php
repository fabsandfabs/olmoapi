<?php

namespace Olmo\Core\Containers\Storage\Filemanager\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Filemanager extends OlmoMainModel
{

    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Filemanager';
    
}
