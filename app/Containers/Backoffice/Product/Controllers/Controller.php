<?php

namespace App\Containers\Backoffice\Product\Controllers;

use Illuminate\Http\Request;

use App\Containers\Backoffice\Product\Models\Product;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;

class Controller extends BackController
{ 

    public function getPost(Request $request)
    {
        $attribute = Product::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Product::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Product::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function updatePost(Request $request)
    {
    	$attribute = Product::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }       

}
