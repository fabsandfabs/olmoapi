<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_product', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            HelpersMigration::Page($table);
            $table->string('home_is_general')->nullable();
            $table->text('product_img_general')->nullable(); 
            $table->text('cover_img_general')->nullable(); 
            $table->text('color_color_general')->nullable();
            $table->text('video_media_general')->nullable();
            $table->text('gallery_slider_general')->nullable();
            $table->text('selectedby_multid_general')->nullable();
            $table->text('designer_id_general')->nullable();
            $table->text('project_multid_general')->nullable();
            $table->text('product_multid_general')->nullable();
            $table->text('sector_multid_general')->nullable();
            $table->text('property_props_general')->nullable();
            $table->text('plus_multid_general')->nullable();
            $table->text('technology_multid_general')->nullable();  
            // Blocks
            // $table->text('blocks_visual_blocks')->nullable(false);
            // SEO
            HelpersMigration::Seo($table);
            // Sitemap
            HelpersMigration::Sitemap($table);

            // Content
            $table->text('title_txt_content')->nullable();
            // $table->text('subtitle_editor_content')->nullable();
            $table->text('abstract_editor_content')->nullable();
            $table->text('content1_editor_content')->nullable();
            $table->text('content2_editor_content')->nullable();
            $table->text('brochure_media_content')->nullable();
            $table->text('energy_media_content')->nullable();
            // Configurator
            $table->text('blocks_visual_configurator')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_product');
    }
}
