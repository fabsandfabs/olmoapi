<?php

namespace Olmo\Core\App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

use Olmo\Core\App\Helpers\HelpersUser;

class HelpersPermission
{

    private const CONTAINERS_DIRECTORY_NAME = 'app/Settings'; 

    private const CONTAINER_SECTION = 'Menu';    

    public static function getMenu(Request $request)
    {

      $menuDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_SECTION . DIRECTORY_SEPARATOR);
      $directory = File::isDirectory($menuDirectory);

      if($directory){

        $token = $request->header('x-token');
        $user = self::userRole($token);

        if(!$user){
          return response(['No user provide'], 403);
        }

        $menuPath = $menuDirectory . 'menu.json';
        $menuRaw = json_decode(file_get_contents($menuPath), true);

        return response(self::userPermissionMenu($menuRaw, intval($user)), 200);        
      }

      return response(['No directory provide'], 400);

    }     

    public static function userPermissionMenu($data, $user)
    {

      $menu = [];

      if($user > 1){
        $data = self::adminPermissions($data);
      }
      if($user > 2){
        $data = self::editorPermissions($data, 'customer');
        $data = self::editorPermissions($data, 'property');
      }

      return $data;
    } 

    public static function userPermissionFields($data)
    {
        return self::publisherPermissions($data);
    }    

    public static function userRole($token)
    {

      $getUser = Db::table('olmo_user')->where('token_hidden_general', $token)->first();
      $userRole = null;
      if($getUser){
        $userRole = $getUser->role_id_general;
      }
      
      return $userRole;

    }    

    public static function adminPermissions($data)
    {
      foreach($data as $key=>$label){
        if($label['subheader'] == 'setting'){
          array_splice($data, $key, 1);
        }        
      }

      return $data;
    }

    public static function editorPermissions($data, $value)
    {
      foreach($data as $key=>$label){
        if($label['subheader'] == 'website'){
          foreach($label['items'] as $index=>$item){
            if($item['name'] == $value){
              array_splice($data[$key]['items'], $index, 1);
            }
          }
        }        
      }

      return $data;
    } 

    /**
     * This must be fixed, originally the publisherPermission method should take the seo and the sitemap 
     * tabs out of the post array
     */
    public static function publisherPermissions($data)
    {

        $user = HelpersUser::getUser();        
        
        $array = $data[0];

        if(isset($user->role_id_general)){
          if(intval($user->role_id_general) > 3){
            foreach($array as $key=>$value){
                if(strpos($key, '_seo') !== false){
                    unset($array[$key]);
                }
                if(strpos($key, '_sitemap') !== false){
                    unset($array[$key]);
                }                
            }
          }
        } else {
          return false;
        }

        $temp = [];
        $temp[] = $array;        

        return $temp;
    }    

}