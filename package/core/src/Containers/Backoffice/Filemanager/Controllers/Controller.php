<?php

namespace Olmo\Core\Containers\Backoffice\Filemanager\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Http\Controller\StorageController;
use Olmo\Core\App\Http\Controller\UploadController;


class Controller extends BackController
{

    use StorageController, UploadController;

    public function getAllFiles()
    {
        return $this->serializeAllFile();
    }

    public function fileEdit(Request $request)
    {
        return $this->fileEditMethod($request);
    }

    public function folderEdit(Request $request)
    {
        return $this->folderEditMethod($request);
    }

    public function moveFiles(Request $request)
    {
        return $this->moveFilesMethod($request);
    }

    public function deleteFile(Request $request)
    {
        return $this->deleteFileMethod($request);
    }

    public function uploadChunks(Request $request)
    {
        return $this->uploadChunk();
    }

    public function uploadComplete(Request $request)
    {
        return $this->completeChunkManager();
    }

    public function folderCreate(Request $request)
    {
        return $this->folderCreateMethod($request);
    }

    public function fileMove(Request $request)
    {
        return $this->fileMoveMethod();
    }

    public function getToken(Request $request)
    {
        $data = [
            'token'     => md5(time() . 'tokenolmo'),
            'model'     => $request->model,
            'modelid'   => $request->modelid,
            'created_at' => date('Y-m-d H:i:s')
        ];

        return Db::table('olmo_tokens')->insert($data);
    }    

    /**
     * ---------------------------
     * WORK IN PROGRESS
     * ---------------------------
     */
    public function moveFolder()
    {
        // $foldersrc   = 'uno/due';
        // $folderdest  = 'uno';
        // $src         = storage_path('app/public/media/' . $foldersrc);
        // $dest        = storage_path('app/public/media/' . $folderdest);

        // return Storage::move($src, $dest);
    }   

    public function completeChunk(Request $request)
    {

        $chunk_id     = @$request->id;
        $filename     = @$_REQUEST['fileName'];
        $truename     = @$_REQUEST['trueName'];
        $fieldname    = @$_REQUEST['fieldName'];
        $current_data = Cache::get($filename);

        $pagename     =  $request->pagename;
        $type = pathinfo($truename, PATHINFO_EXTENSION);

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
        ];
        
    
        $path  = 'public/'.$categories_files[$type].'/'.$pagename.'/'.$filename;
        $res   = Storage::put($path, $current_data);

        $public_path = '/'.$categories_files[$type].'/'.$pagename.'/'.$truename;

        if($res){
            $res = true;
        }else{
            $res = false;
        }
       
        $storage_id = DB::table('olmo_storage')->insertGetId([
           'filename' 	=> $filename,
           'truename' 	=> $truename,
           'model'     => $pagename,
           'meta'     => $chunk_id.$fieldname,
           'public'    => 'true',
           'type'      => $type
        ]);


        if(strpos($fieldname,'_multimg')    !== false) { 

           //storage id incrementale
           $entity     = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->first();

           if($entity){
              $storage_id = $entity->{$fieldname}.','.$storage_id;
              $storage_id = trim($storage_id,",");
           }

           $update = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->update([
              $fieldname 	=> $storage_id
           ]);   

        }else{
           $update = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->update([
              $fieldname 	=> $storage_id
           ]);
        }

        $arr['data']        = $public_path;
        $arr['id']        = $storage_id;
        $arr['isSuccess']   = $res;
        $arr['isUpdate']   = $update;
        $arr['errorMessage'] = null;

        Cache::forget($filename);
        return $arr;

    }    
}
