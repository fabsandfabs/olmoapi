<?php

namespace App\Containers\Backoffice\Arredo\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class ArredoRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
