<?php

use App\Containers\Backoffice\Product\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/product', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the product post
Route::get('{lang}/product/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the product post
Route::put('{lang}/product/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the product post
Route::post('{lang}/product/create', [Controller::class, 'createPost'])->middleware(['auth:api']);