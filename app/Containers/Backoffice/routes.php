<?php 

// Backoffice routes import
require "Page/routes.php";
require "Product/routes.php";
require "Sector/routes.php";
require "Designer/routes.php";
require "Project/routes.php";
require "Blog/routes.php";
require "Plus/routes.php";
require "Selectedby/routes.php";
require "Arredo/routes.php";
require "Formatpers/routes.php";
require "Whyifi/routes.php";
require "Technology/routes.php";
require "Openposition/routes.php";