<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\General\Controllers\Controller;

// Get the list
Route::post('{lang}/language', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the general post
Route::get('{lang}/language/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);
Route::get('{lang}/analytics', [Controller::class, 'getAnalytic'])->middleware(['auth:api']);

// Update the general post
Route::put('{lang}/language/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);
Route::put('{lang}/analytics', [Controller::class, 'updateAnalytic'])->middleware(['auth:api']);

// Create the general post
Route::post('{lang}/language/create', [Controller::class, 'createPost'])->middleware(['auth:api']);

// Get current column list for translate porpuse
Route::get('thetable/{table}', [Controller::class, 'theTable'])->middleware(['auth:api']);

// Copy the requested field in the new post
Route::post('{lang}/thetable/{table}/{id}', [Controller::class, 'copyColumns'])->middleware(['auth:api']);


/**
 * Just for management porpuse
 */
// Route::get('_/containersBackoffice/list', [Controller::class, 'containersList'])->middleware(['auth:api']);
// Route::get('_/clearTable/toempty', [Controller::class, 'toEmpty'])->middleware(['auth:api']);
// Route::get('_/fixcolumvisual', [Controller::class, 'fixcolumn'])->middleware(['auth:api']);