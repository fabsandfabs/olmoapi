<?php

use App\Containers\Backoffice\Openposition\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/openposition', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the openposition post
Route::get('{lang}/openposition/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the openposition post
Route::put('{lang}/openposition/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the openposition post
Route::post('{lang}/openposition/create', [Controller::class, 'createPost'])->middleware(['auth:api']);