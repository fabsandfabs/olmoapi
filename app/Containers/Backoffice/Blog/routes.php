<?php

use App\Containers\Backoffice\Blog\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/blog', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the blog post
Route::get('{lang}/blog/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the blog post
Route::put('{lang}/blog/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the blog post
Route::post('{lang}/blog/create', [Controller::class, 'createPost'])->middleware(['auth:api']);