<?php

use App\Containers\Backoffice\Selectedby\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/selectedby', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the selectedby post
Route::get('{lang}/selectedby/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the selectedby post
Route::put('{lang}/selectedby/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the selectedby post
Route::post('{lang}/selectedby/create', [Controller::class, 'createPost'])->middleware(['auth:api']);