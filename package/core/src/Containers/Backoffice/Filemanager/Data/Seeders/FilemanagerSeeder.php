<?php

namespace Olmo\Core\Containers\Backoffice\Filemanager\Data\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilemanagerSeeder extends Seeder
{
    public function run()
    {

		DB::table('olmo_storage')->insert([
			'filename' 	=> 'f42f9868-750b-4d06-a05a-f57569d085df.jpg',
			'truename' 	=> 'home/menu01.jpg',
			'model' 	=> 'filemanager',
			'type'  	=> 'jpg',
			'public'  	=> 'true'
		]);      

    }
}
