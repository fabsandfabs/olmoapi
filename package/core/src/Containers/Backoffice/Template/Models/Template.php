<?php

namespace Olmo\Core\Containers\Backoffice\Template\Models;

use Olmo\Core\App\Models\OlmoMainModel;

class Template extends OlmoMainModel
{
    public $timestamps = false;

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Template';

    public $table = 'olmo_template';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_template',
            'required' => [],
            'lang' => false,
            'listing' => 'name_txt_general, slug_txt_general, model_select_general as model_txt_general, enabled_is_general, id'
        ];
    }    
}
