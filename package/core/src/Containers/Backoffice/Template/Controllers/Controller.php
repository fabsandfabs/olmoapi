<?php

namespace Olmo\Core\Containers\Backoffice\Template\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\Containers\Backoffice\Template\Models\Template;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;

class Controller extends BackController
{

    public function getPost(Request $request)
    {
        $attribute = Template::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = Template::theAttributes();
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = Template::theAttributes();

        return $this->createNewPost($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Template::theAttributes();

        return $this->serializePostUpdating($request, $attribute);
    }  


}