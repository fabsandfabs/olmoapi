<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Olmo\Core\App\Helpers\HelpersMigration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('olmo_page', function (Blueprint $table) {
            // Create new table...
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            // General
            HelpersMigration::Page($table);
            $table->text('page_id_general')->nullable(false);
            $table->text('slidermain_slider_content')->nullable(false);
            $table->text('title_txt_content')->nullable(false);
            $table->text('subtitle_editor_content')->nullable(false);
            $table->text('abstract_editor_content')->nullable(false);
            $table->text('slider_slider_content')->nullable(false);                
            $table->text('cover_filemanager_content')->nullable(false);
            $table->text('video_filemanager_content')->nullable(false);
            $table->text('cta_txt_content')->nullable(false);
            $table->text('content_editor_content')->nullable(false);       
            // Blocks
            // $table->text('blocks_visual_blocks')->nullable();
            // SEO
            HelpersMigration::Seo($table);
            // Sitemap
            HelpersMigration::Sitemap($table); 
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_page');
    }
}
