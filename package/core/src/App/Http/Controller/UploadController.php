<?php

namespace Olmo\Core\App\Http\Controller;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Helpers\HelpersData;

trait UploadController
{

   public static function uploadChunk()
   {

         $chunk_id     = @$_REQUEST['id'];
         $filename     = @$_REQUEST['fileName'];
         $data         = file_get_contents('php://input');
         $temp_data    = Cache::get($filename);
         $current_data = $temp_data.$data;
         Cache::put($filename,$current_data,1200);

         $arr['data']         = null;
         $arr['isSuccess']    = true;
         $arr['errorMessage'] = null;

         return $arr;
    }

    public static function completeChunkManager()
    {

         $filename      = @$_REQUEST['fileName'];
         $truename      = HelpersData::normalizestring($_REQUEST['trueName']);
         $folders       = @$_REQUEST['foldersName'];
         $current_data  = Cache::get($filename);
         $path  		= 'public/media/'.$folders.'/'.$filename;
         $res           = Storage::put($path, $current_data);
         $public_path   = $folders !== '' ? '/media/'.$folders.$truename : '/media/'.$truename;

         if($res){
            $res = true;
         }else{
            $res = false;
         }
      
        $truename   = $folders.$truename;
        $checkTrueName =  DB::table('olmo_storage')->where('truename',$truename)->get()->count();

        $storage_id = DB::table('olmo_storage')->insertGetId([
           'filename' 	=> $filename,
           'truename' 	=> $truename,
           'model'      => 'filemanager',
           'public'     => 'true',
           'type'       => pathinfo($truename)['extension'],
        ]); 

        if($checkTrueName > 0){
            $truename   = explode('.',$truename);
            $newTruename = $truename[0].'-'.$storage_id.'.'.$truename[1];
            DB::table('olmo_storage')->where('id',$storage_id)->update(['truename' => $newTruename]);
        }



        $arr['data']         = $public_path;
        $arr['id']           = $storage_id;
        $arr['isSuccess']    = $res;
        $arr['errorMessage'] = null;
        Cache::forget($filename);
        return $arr;
    }   

}