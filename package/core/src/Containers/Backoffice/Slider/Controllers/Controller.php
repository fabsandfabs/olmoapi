<?php

namespace Olmo\Core\Containers\Backoffice\Slider\Controllers;

use Illuminate\Http\Request;

use Olmo\Core\Containers\Backoffice\Slider\Models\Slider;
use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Helpers\HelpersSlider;

class Controller extends BackController
{

    public function getPost(Request $request)
    {
        $attribute = Slider::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        HelpersExtra::managerExtra($request, $attribute);

        return HelpersSlider::serializeSingleSlide($request, $attribute);
    }

    // public function getPosts(Request $request)
    // {
    //     $attribute = Slider::theAttributes();        
    //     $dataPost = HelpersData::idLangPost($request);
        
    //     return $this->serializeListPosts($attribute, $dataPost);
    // }

    public function createPost(Request $request)
    {
        $attribute = Slider::theAttributes();

        return HelpersSlider::createNewSlide($request, $attribute);
    }

    public function updatePost(Request $request)
    {
    	$attribute = Slider::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return HelpersSlider::serializeSlideUpdating($request, $attribute, $dataPost);
    }   

}
