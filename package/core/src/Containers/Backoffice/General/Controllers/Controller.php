<?php

namespace Olmo\Core\Containers\Backoffice\General\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Core\App\Http\Controller\BackController;
use Olmo\Core\Containers\Backoffice\General\Models\General;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\Loaders\OlmoLoadersContainers;

class Controller extends BackController
{

    public function updatePost(Request $request)
    {
    	$attribute = General::theAttributes();       

        return $this->serializePostUpdating($request, $attribute);
    }

    public function getPost(Request $request)
    {
        $attribute = General::theAttributes();
        $dataPost = HelpersData::idLangPost($request);

        return  $this->serializeSinglePost($attribute, $dataPost);
    }

    public function getPosts(Request $request)
    {
        $attribute = General::theAttributes();        
        $dataPost = HelpersData::idLangPost($request);
        
        return $this->serializeListPosts($attribute, $dataPost);
    }

    public function updateAnalytic(Request $request)
    {
        $attribute  =  [
            'table' => 'olmo_analytics',
            'required' => []
        ];
        $dataPost = [
            'id' => 1,
            'lang' => $request->lang
        ];
        return $this->serializePostUpdating($request, $attribute);
    }   

    public function getAnalytic(Request $request)
    {
        $attribute  =  [
            'table' => 'olmo_analytics',
            'required' => []
        ];
        $dataPost = [
            'id' => 1,
            'lang' => $request->lang
        ];

        return  $this->serializeSinglePost($attribute, $dataPost);
    }

    public function createPost(Request $request)
    {
        $attribute = General::theAttributes();

        return $this->createNewPost($request, $attribute);
    }    

    public function theTable(Request $request)
    {
        return HelpersExtra::theTable($request);
    }    

    public function copyColumns(Request $request)
    {
        return HelpersExtra::copyColumnsFromDefault($request);
    }

    /**
     * Just for management porpuse
     */
    // public function containersList()
    // {
    //     $containers = OlmoLoadersContainers::getAllContainerPaths();
    //     $model = [];

    //     $empty = array('key' => '', 'value' => '');
    //     array_push($model, $empty);

    //     foreach($containers as $item){
    //         $container = last(explode('/', $item));
    //         $value = [];
    //         $value['key'] = strtolower($container);
    //         $value['value'] = strtolower($container);
    //         array_push($model, $value);
    //     }

    //     return $model;
    // }

    // public function toEmpty()
    // {
    //     $table = Db::table('olmo_visualblock')->get();

    //     foreach($table as $row){
    //         array_walk($row, function (&$v, $k, $alter) {
    //             if (is_null($v)) {
    //                 $v = $alter;
    //             }
    //         }, '');       
            
    //         Db::table('olmo_visualblock')->where('id', $row->id)->update((array)$row);
    //     }

    //     $table = Db::table('olmo_visualblock')->get();

    //     return $table;
    // }

    // public function fixcolumn()
    // {

    //     $data = DB::table('olmo_product')->get();
    //     foreach($data as $item){
    //         $ids = $item->technics_visual_technics;
    //         $ids = explode(',', $ids);
    //         foreach($ids as $id) {
    //             $data = DB::table('olmo_visualblock')->where('id', $id)->update(['postid_hidden_content' => $item->id]);
    //         }
    //     }        
        

    //     return response(200);
    // }    

}