<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\Template\Controllers\Controller;

// Get the list
Route::post('{lang}/template', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the template post
Route::get('{lang}/template/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the template post
Route::put('{lang}/template/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the template post
Route::post('{lang}/template/create', [Controller::class, 'createPost'])->middleware(['auth:api']);