<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Frontend\Base\Controllers\Controller;

// Route::get('{lang}/allcategories', [Controller::class, 'getAllcategories']);
Route::get('{lang}/allmodel/{model}', [Controller::class, 'getAllPosts']);
Route::get('structure', [Controller::class, 'getStructure']);
Route::get('sitemap/{lang}', [Controller::class, 'getSitemap']);
Route::get('robots.txt', [Controller::class, 'getRobots']);

/** Fitelrs */
Route::post('{lang}/filters/{model}', [Controller::class, 'filterPost']);

/** Base */
Route::get('{lang}/{category?}/{subcategory?}/{slug?}', [Controller::class, 'getPage']);