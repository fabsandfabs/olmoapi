<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_user')) {
            Schema::table('olmo_user', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_user', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->string('enabled_is_general', 5)->nullable(false);
                $table->text('name_txt_general')->nullable(false);
                $table->text('email_email_general')->nullable(false);
                $table->text('password_pwd_general')->nullable(false);
                $table->text('confirmpassword_pwd_general')->nullable(false);
                $table->text('role_id_general')->nullable(false);
                $table->text('avatar_filemanager_general')->nullable(false);
                $table->text('token_hidden_general')->nullable(false);
            });

            Schema::create('olmo_role', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->text('label_txt_general')->nullable(false);
                $table->text('name_txt_general')->nullable(false);
            });            
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_user');
    }
}
