<?php

namespace Olmo\Core\App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use DirectoryIterator;

class OlmoRoutesServiceProvider extends RouteServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {       

           /**
            * Project routes declared inside the project forlder Containers/Backoffice
            * */
            Route::prefix('backoffice')    
                ->middleware('backoffice')
                ->namespace($this->namespace)
                ->group(base_path('app/Containers/Backoffice/routes.php'));

           /**
            * Olmo standard routes for backoffice porpuse
            * */
            Route::prefix('backoffice')
                ->middleware('backoffice')
                ->namespace($this->namespace)
                ->group(__DIR__ . '/../../Containers/Backoffice/routes.php');

           /**
            * Olmo standard routes for storage porpuse
            * */
            Route::prefix('storage')
                ->middleware('backoffice')
                ->namespace($this->namespace)
                ->group(__DIR__ . '/../../Containers/Storage/routes.php');

           /**
            * Olmo standard routes for frontend porpuse
            * */
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(__DIR__ . '/../../Containers/Frontend/routes.php');                

           /**
            * Olmo welcome route
            * */                
            // Route::middleware('web')
            //     ->namespace($this->namespace)
            //     ->group(__DIR__ . '/../../Containers/Frontend/Welcome/routes.php');

        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
