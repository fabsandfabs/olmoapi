<?php

namespace Olmo\Core\App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;
use Olmo\Core\Loaders\OlmoLoadersContainers;

class OlmoMigrationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadMigrationsFromContainers();
        $this->defaultLoadMigrationsFromContainers();

    }    

    public function register()
    {
        //
    }

    /**
     * This is something must be changed
     * Load the containers the user create
     */
    public function loadMigrationsFromContainers()
    {
        foreach (OlmoLoadersContainers::getAllContainerPaths() as $containerPath) {
            $containerMigrationPath = $containerPath."/Data/Migrations";       
            if(File::isDirectory($containerMigrationPath)){
                $this->loadMigrationsFrom($containerMigrationPath);
            }            
        }     
    } 
    
    /**
     * Load the default containers
     */    
    public function defaultLoadMigrationsFromContainers()
    {
        foreach (OlmoLoadersContainers::getAllDefaultContainerPaths() as $containerPath) {
            $containerMigrationPath = $containerPath."/Data/Migrations";       
            if(File::isDirectory($containerMigrationPath)){
                $this->loadMigrationsFrom($containerMigrationPath);
            }            
        }     
    }     
   
}