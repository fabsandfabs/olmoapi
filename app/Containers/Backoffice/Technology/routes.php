<?php

use App\Containers\Backoffice\Technology\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
Route::post('{lang}/technology', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the technology post
Route::get('{lang}/technology/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the technology post
Route::put('{lang}/technology/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the technology post
Route::post('{lang}/technology/create', [Controller::class, 'createPost'])->middleware(['auth:api']);