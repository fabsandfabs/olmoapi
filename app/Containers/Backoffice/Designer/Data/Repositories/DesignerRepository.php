<?php

namespace App\Containers\Backoffice\Designer\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class DesignerRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
