<?php

use Illuminate\Support\Facades\Route;
use Olmo\Core\Containers\Backoffice\VisualBlock\Controllers\Controller;

Route::post('{lang}/{model}/visual/{model_field}/{id}', [Controller::class, 'addVisual'])->middleware(['auth:api']);

Route::get('visual', [Controller::class, 'getComponentVisual'])->middleware(['auth:api']);

Route::put('{lang}/{model}/{model_id}/visual/{name}/{id}', [Controller::class, 'saveVisual'])->middleware(['auth:api']);