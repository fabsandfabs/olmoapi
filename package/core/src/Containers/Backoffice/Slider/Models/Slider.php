<?php

namespace Olmo\Core\Containers\Backoffice\Slider\Models;

use Olmo\Core\App\Models\OlmoMainModel;

// use Illuminate\Support\Facades\DB;

class Slider extends OlmoMainModel
{

    public $timestamps = false;
    
    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Slider';

    public $table = 'olmo_slider';
    
    public static function theAttributes(){
        return [
            'table' => 'olmo_slideritem',
            'required' => []
        ];
    }

}
