<?php

use Olmo\Core\Containers\Backoffice\Slider\Controllers\Controller;
use Illuminate\Support\Facades\Route;

// Get the list
// Route::post('{lang}/slider', [Controller::class, 'getPosts'])->middleware(['auth:api']);

// Get the page post
Route::get('{lang}/slider/{id}', [Controller::class, 'getPost'])->middleware(['auth:api']);

// Update the slider post
Route::put('{lang}/slider/{id}', [Controller::class, 'updatePost'])->middleware(['auth:api']);

// Create the slider post
Route::post('{lang}/slider/create', [Controller::class, 'createPost'])->middleware(['auth:api']);